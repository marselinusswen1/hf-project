from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import TimeoutException, NoSuchElementException
import undetected_chromedriver as uc

import time
import random
# import logging
# logging.basicConfig(level=10)

import os
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-u", "--username", help="Username")
parser.add_argument("-p", "--password", help="Password")

repo = "https://huggingface.co/spaces/hfsgsh5/gaguguga1?duplicate=true"
args = parser.parse_args()

# Inisialisasi undetected-chromedriver
options = uc.ChromeOptions()
options.add_argument("--no-sandbox")
options.add_argument("--disable-notifications")
options.add_argument("--window-size=1382,736")
options.add_argument('--disable-dev-shm-usage')

# Inisialisasi WebDriver
driver = uc.Chrome(pptions=options, version_main=113)
print("+++++++++++++++++++++++++++++++++++++++++++++++", flush=True)
print("[✓] OPEN UC CHROME NO GUI AND NO SANDBOX", flush=True)
print("+++++++++++++++++++++++++++++++++++++++++++++++", flush=True)

# LOGIN
driver.get('https://huggingface.co/login')
print("===============================================", flush=True)
print("[+] LOGIN", flush=True)
WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, "//input[@name='username']")))
time.sleep(2)
driver.set_window_size(1382, 736)
print("[+] SET WINDOW SIZE", flush=True)
email = WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//input[@name='username']")))
email.send_keys(args.username)
print("[+] INPUT USERNAME", flush=True)
time.sleep(2)
password = WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//input[@name='password']")))
password.send_keys(args.password, Keys.ENTER)
print("[+] INPUT PASSWORD", flush=True)
# ===========================================

while True:
    print("===============================================", flush=True)
    # RANDOM KODE
    lantak = random.randint(11111111,99999999)
    print(f"[-] Random Kode : {lantak}.")
    # ===========================================

    # CREATE ORGANIZATION
    try:
        driver.get('https://huggingface.co/organizations/new')
        print("===============================================", flush=True)
        print("[+] CREATE ORGANIZATION", flush=True)
        WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, "/html/body/div/main/div/section/div/form/div[2]/label[1]/input")))

        org_user = WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div/main/div/section/div/form/div[2]/label[1]/input')))
        org_user.send_keys('hfshsg' + str(lantak))
        print(f"[+] INPUT ORGANIZATION USERNAME = hfshsg {str(lantak)}")
        time.sleep(1)
        fullname = WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div/main/div/section/div/form/div[2]/label[2]/input')))
        fullname.send_keys('hfshsg' + str(lantak), Keys.ENTER)
        print(f"[+] CREATE ORGANIZATION SUCCESS")
    except NoSuchElementException:
        print("===============================================", flush=True)
        print("[X] ERROR CREATE ORGANIZATION - NoSuchElementException", flush=True)
        print("===============================================", flush=True)
        break
    except TimeoutException:
        print("===============================================", flush=True)
        print("[X] ERROR CREATE ORGANIZATION - TimeoutException", flush=True)
        print("===============================================", flush=True)
        break
    # ===========================================
    print("===============================================", flush=True)
    time.sleep(2)

    try:
        # CREATE REPO
        driver.get(repo)
        print("[+] CREATE REPO", flush=True)
        WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, "/html/body/div/main/div[1]/header/div/div/div[2]/div[2]/div")))
        time.sleep(2)
        dropdown_select = Select(WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.XPATH, '/html[1]/body[1]/div[1]/main[1]/div[1]/header[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/label[1]/div[1]/select[1]'))))
        dropdown_select.select_by_visible_text('hfshsg' + str(lantak))
        print(f"[+] SELECT ORGANIZATION = hfshsg {str(lantak)}")
        time.sleep(2)
        driver.execute_script("arguments[0].click();", WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/main/div[1]/header/div/div/div[2]/div[2]/div/div[2]/div[3]/button[1]'))))
        time.sleep(3)
        print("[+] CREATE REPO SUCCESS", flush=True)
        print("===============================================", flush=True)
        print("[*] WHILE AGAIN", flush=True)
    except NoSuchElementException:
        print("===============================================", flush=True)
        print("[X] ERROR CREATE REPO - NoSuchElementException", flush=True)
        print("===============================================", flush=True)
        break
    except TimeoutException:
        print("===============================================", flush=True)
        print("[X] ERROR CREATE REPO - TimeoutException", flush=True)
        print("===============================================", flush=True)
        break
    # ===========================================

print(driver.title)
driver.quit()