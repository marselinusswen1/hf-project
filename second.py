from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import TimeoutException, NoSuchElementException
import undetected_chromedriver as uc


import time
import random
# import logging
# logging.basicConfig(level=10)

import os
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-u", "--username", help="Username")
parser.add_argument("-p", "--password", help="Password")
args = parser.parse_args()

# Inisialisasi undetected-chromedriver
options = uc.ChromeOptions()
options.add_argument("--no-sandbox")
options.add_argument("--disable-notifications")
options.add_argument("--window-size=1382,736")
options.add_argument('--disable-dev-shm-usage')

# Inisialisasi WebDriver
driver = uc.Chrome(options=options, version_main=113)
print("+++++++++++++++++++++++++++++++++++++++++++++++", flush=True)
print("[✓] OPEN UC CHROME NO GUI AND NO SANDBOX", flush=True)
print("+++++++++++++++++++++++++++++++++++++++++++++++", flush=True)

# LOGIN
driver.get('https://huggingface.co/login')
print("===============================================", flush=True)
print("[+] LOGIN", flush=True)
WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, "//input[@name='username']")))

time.sleep(2)
driver.set_window_size(1382, 736)
print("[+] SET WINDOW SIZE", flush=True)
email = WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//input[@name='username']")))
email.send_keys(args.username)
print("[+] INPUT USERNAME", flush=True)
time.sleep(2)
password = WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//input[@name='password']")))
password.send_keys(args.password, Keys.ENTER)
print("[+] INPUT PASSWORD", flush=True)
# ===========================================


# RANDOM KODE
lantak = random.randint(11111111,99999999)
print(f"[*] Random Kode : {lantak}.")
# ===========================================

while True:
    link_href = {}
    # CHECK IF NOT STATUS BUILDING
    driver.get("https://huggingface.co/")
    print("===============================================", flush=True)
    print("[+] GET https://huggingface.co/", flush=True)
    WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, "/html/body/div[1]/main/div/div/div[2]/div[2]/div/div[2]")))
    time.sleep(2)
    count = 0
    count_build_error = 0
    print("[+] CHECK IF NOT STATUS BUILDING", flush=True)
    while True:
        try:
            count += 1
            xpath = "/html/body/div[1]/main/div/div/div[2]/div[2]/div/div[2]/article[" + str(count) + "]/a/div[1]/div[2]"
            xpath2 = "/html/body/div[1]/main/div/div/div[2]/div[2]/div/div[2]/article[" + str(count) + "]/a"
            element = WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.XPATH, xpath)))
            if element.text == "Build error":
                element2 = WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.XPATH, xpath2)))
                href = element2.get_attribute("href")
                link_href[count] = href + '/settings'
                count_build_error += 1
                # =====================================================
        except NoSuchElementException:
            print("===============================================", flush=True)
            print("[-] TIDAK ADA BUILD ERROR LAGI TERDETEKSI", flush=True)
            print("[-] JUMLAH XPATH ARTICLE : " + str(count - 1), flush=True)
            print("[-] JUMLAH BUILD ERROR : " + str(count_build_error), flush=True)
            print("===============================================", flush=True)
            count = 0
            break
        except TimeoutException:
            print("===============================================", flush=True)
            print("[-] TIDAK ADA BUILD ERROR LAGI TERDETEKSI", flush=True)
            print("[-] JUMLAH XPATH ARTICLE : " + str(count - 1), flush=True)
            print("[-] JUMLAH BUILD ERROR : " + str(count_build_error), flush=True)
            print("===============================================", flush=True)
            count = 0
            break
    # ===========================
    try:
        # PRINT LINK HREF
        # print(link_href)
        # ===========================

        # RESTART IF STATUS NOT BUILDING
        if count_build_error == 0:
            print("[!] WHILE AGAIN", flush=True)
            continue
        else:
            print(f"[!] RESTART FOR BUILD ERROR TOTAL : {count_build_error}") 
            print("===============================================", flush=True)
            for index, href in link_href.items():
                driver.get(href)
                print(f"[+] DRIVER GET {href}")
                WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, "/html/body/div/main/div[2]/div/section/div/section[2]/div[2]/form/button")))
                driver.execute_script("arguments[0].click();", WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/main/div[2]/div/section/div/section[2]/div[2]/form/button'))))
                time.sleep(3)
            # ===========================
            print("[!] WHILE AGAIN", flush=True)
    except Exception as e:
        print(e)
        print("[-] LINK HREF EMPTY", flush=True)
        print("[!] WHILE AGAIN", flush=True)
        continue

driver.quit()