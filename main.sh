#!/bin/bash

while getopts ":u:p:" opt; do
  case $opt in
    u) username="$OPTARG"
    ;;
    p) password="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

while true; do
  python main.py -u "$username" -p "$password"

  # Jeda selama 5 menit
  echo "Sleeping for 15 minutes..."
  sleep 900
done
