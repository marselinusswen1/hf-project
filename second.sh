#!/bin/bash

while getopts ":u:p:" opt; do
  case $opt in
    u) username="$OPTARG"
    ;;
    p) password="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

while true; do
  python second.py -u "$username" -p "$password"

done
